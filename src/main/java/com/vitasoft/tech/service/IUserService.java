package com.vitasoft.tech.service;

import java.util.Optional;

import com.vitasoft.tech.model.User;

public interface IUserService {
	
	Integer saveUser(User user);
	Optional<User> findByUserName(String username);
}
