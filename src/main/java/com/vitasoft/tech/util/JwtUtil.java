package com.vitasoft.tech.util;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil {

	@Value("${app.secret}")
	private String secret;

//	1.Generate Token

	public String generateToken(String subject) {
		return Jwts.builder().setSubject(subject).setIssuer("Vitasoft")
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(15)))
				.signWith(SignatureAlgorithm.HS512, secret.getBytes()).compact();
	}

//	2.Read Claims(token)

	public Claims getClaims(String token) {
		return Jwts.parser()
				.setSigningKey(secret.getBytes())
				.parseClaimsJws(token)
				.getBody();
	}

//	3.Read Expiry date

	public Date getExpiryDate(String token) {
		return getClaims(token).getExpiration();
	}
//	4.Read Subject/UserName

	public String getUserName(String token) {
		return getClaims(token).getSubject();

	}
	
//	5.Validate Exp date
	
	public boolean isTokenExpired(String token) {
		Date expiryDate=getExpiryDate(token);
		return expiryDate.before(new Date(System.currentTimeMillis()));
	}
	
//	6.Validate user name in token and database ,expirydate
	
	public boolean validateToken(String token,String username) {
		String tokenUserName=getClaims(token).getSubject();
		return (username.equals(tokenUserName) && !isTokenExpired(token));
	}
	
	

}
