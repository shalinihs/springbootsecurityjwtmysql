package com.vitasoft.tech.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.model.User;

public interface UserRepository extends JpaRepository<User,Integer> {
	
	Optional<User> findByUserName(String username);

}
