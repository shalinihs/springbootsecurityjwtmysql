package com.vitasoft.tech.serviceImpl;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.vitasoft.tech.model.User;
import com.vitasoft.tech.repository.UserRepository;
import com.vitasoft.tech.service.IUserService;

@Service
public class UserServiceImpl implements IUserService, UserDetailsService {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private BCryptPasswordEncoder pswEncoder;

//save user
	@Override
	public Integer saveUser(User user) {
		// Encode Password

		user.setPassword(pswEncoder.encode(user.getPassword()));
		return userRepo.save(user).getId();
	}

//get user by user name
	@Override
	public Optional<User> findByUserName(String username) {

		return userRepo.findByUserName(username);
	}

//	Spring Security method not IUserMethod
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> opt= findByUserName(username);
		if(!opt.isPresent()) {
			throw new UsernameNotFoundException("User not Exist");
		}
//		Read user from DB
		User user=opt.get();
		
		
		return new org.springframework.security.core.userdetails.User(
				username,
				user.getPassword(),
				user.getRoles().stream()
				.map(role->new SimpleGrantedAuthority(role))
				.collect(Collectors.toList()));
	}

}
