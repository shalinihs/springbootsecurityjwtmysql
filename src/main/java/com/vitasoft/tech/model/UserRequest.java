package com.vitasoft.tech.model;

import lombok.Data;

@Data
public class UserRequest {
	
	private String userName;
	private String password;
	
}
