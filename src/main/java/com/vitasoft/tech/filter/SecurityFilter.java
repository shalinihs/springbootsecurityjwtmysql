package com.vitasoft.tech.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.vitasoft.tech.util.JwtUtil;

/*
 * Define one filter which gets  executed for every request
 * If Request is having Authorization header, then validate using JwtUtil
 * If valid then create Authentication inside SecurityContext
 * Use oncePerRequest(Abstract Class)*/


@Component  //to create Object
public class SecurityFilter extends OncePerRequestFilter{
	@Autowired
	private JwtUtil util;
	
	@Autowired
	private UserDetailsService userDetailService;

	@Override
	protected void doFilterInternal(
			HttpServletRequest request,
			HttpServletResponse response,
			FilterChain filterChain)
			throws ServletException, IOException {
//		1.Read Token from Auth head
		String token=request.getHeader("Authorization");
		if(token!=null) {
//			Do Validation
			String userName=util.getUserName(token);
			if(userName!=null && SecurityContextHolder.getContext().getAuthentication()==null) {
				UserDetails usr=userDetailService.loadUserByUsername(userName);
				boolean isValid=util.validateToken(token, usr.getUsername());
				if(isValid) {
					UsernamePasswordAuthenticationToken authToken=new UsernamePasswordAuthenticationToken(userName,usr.getPassword(),usr.getAuthorities());
					authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				//	Final Object Stored in SecurityContext with user details (username ,password)
					SecurityContextHolder.getContext().setAuthentication(authToken);
				}
			}
		}
//		Continue FilterChain goto next filter or  servlet(here next front controller) or next filterchain
		filterChain.doFilter(request, response);
		
		
	}

}
